﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Bullet
{
   void Start()
    {
        bullet.velocity = transform.up * speed; 
        Destroy(gameObject, 5);
    }
}
