﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Store_UI : MonoBehaviour
{

    public Text scoreText;
    private int playerScore;

    private void Start()
    {
        playerScore = PlayerPrefs.GetInt("Score");
    }

    private void Update()
    {
        scoreText.text = "Score: " + playerScore.ToString();
    }

    public void updateScore()
    {
        playerScore = PlayerPrefs.GetInt("Score");
        scoreText.text = "Score: " + playerScore.ToString();
    }
}
