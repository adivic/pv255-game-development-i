﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public int score;
    public int round;

    // Start is called before the first frame update
    void Start()
    {
        score = loadScore();
        round = 1;
    }

    public void addScore(int scoreDelta) {
        score = loadScore();
        score += scoreDelta;
        saveScore();
    }

    public int updateRound() { return ++round; }

    public void saveScore() {
        PlayerPrefs.SetInt("Score", score);
    }

    public int loadScore()
    {
        return PlayerPrefs.GetInt("Score");
    }
}
