﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_Menu : MonoBehaviour
{
    public Button level2Button;
    public Image lockSprite;

    public AudioClip hover;
    public AudioClip click;

    public GameMode gameMode;

    void Start() {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            if (PlayerPrefs.HasKey("UnlockedLevel") && PlayerPrefs.GetInt("UnlockedLevel") == 1)
            {
                level2Button.interactable = true;
                lockSprite.gameObject.SetActive(false);
            }
            else
            {
                level2Button.interactable = false;
                lockSprite.gameObject.SetActive(true);
            }
        }
    }

    public void PlayGame(int level)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + level);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit!");
    }

    public void RestartGame()
    {
        Time.timeScale = 1.0f;
        gameMode.ClearScene(); 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        Time.timeScale = 1.0f;
        gameMode.ClearScene();
        SceneManager.LoadScene(0);
    }

    public void PlayButtonSound(bool bClick) {

        AudioSource source = GetComponent<AudioSource>();
        if (bClick)
        {
            source.clip = click;
            source.volume = 2.0f;
            source.Play();
        }else
        {
            source.clip = hover;
            source.volume = 0.8f;
            source.Play();
        }
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
    }

}
