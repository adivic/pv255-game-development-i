﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class EnemyBoss : Enemy
{
    public GameObject[] bullets;
    public Transform rocketSpawn;

    public AudioClip[] sounds;

    public byte stage;

    void Start()
    {
        enemy = GetComponent<Rigidbody>();
        speed = Random.Range(5, 10);
        nextFire = Time.time;
        size = player.GetComponent<PlayerController>().getScreenSize();
        StartCoroutine(Move());
        randomHeight = Random.Range(10, 14);
        healthComp = GetComponent<HealthComponent>();
        float healthAmout = SceneManager.GetActiveScene().buildIndex == 2 ? 1500 : 750;
        healthComp.setHealth(healthAmout);
        healthComp.defaultHealth = healthAmout;
        Debug.Log("BOSS HEALTH = " + healthAmout); 
        fireRate = Random.Range(1.0f, 2.0f);
        stage = 0;

        audio = GetComponent<AudioSource>();
    }

    override protected void FireProjectile() {
        if (Time.time > nextFire) {
            if (stage == 0) {
                Transform left = GameObject.Find("StarSparrow_Engine_Left").GetComponent<Transform>();
                Transform right = GameObject.Find("StarSparrow_Engine_Right").GetComponent<Transform>();
                GameObject leftBullet = Instantiate(bullets[0], left.position, rocketSpawn.rotation);
                Destroy(leftBullet, 3);
                GameObject rightBullet = Instantiate(bullets[0], right.position, rocketSpawn.rotation);
                Destroy(rightBullet, 3);
                nextFire = Time.time + fireRate;
                audio.clip = sounds[0];
                audio.volume = 0.8f;
                audio.Play();
                audio.loop = false;

            }
            if (stage == 1)
            {
                Transform right = GameObject.Find("StarSparrow_Engine_Right").GetComponent<Transform>();
                GameObject rightBullet = Instantiate(bullets[0], right.position, rocketSpawn.rotation);
                Destroy(rightBullet, 3f);
                nextFire = Time.time + fireRate;
                audio.clip = sounds[0];
                audio.volume = 0.6f;
                audio.loop = false;
                audio.Play();
            }

            if (stage == 2)
            {
                fireRate = 0.3f;
                Transform left = GameObject.Find("StarSparrow_Weapon_Left").GetComponent<Transform>();
                GameObject leftBullet = Instantiate(bullets[1], left.position, spawn.rotation);
                Destroy(leftBullet, 3f);
                Transform right = GameObject.Find("StarSparrow_Weapon_Right").GetComponent<Transform>();
                GameObject rightBullet = Instantiate(bullets[1], right.position, spawn.rotation);
                Destroy(rightBullet, 3);
                nextFire = Time.time + fireRate;
                if(audio.clip != sounds[1] && !audio.isPlaying)
                {
                    audio.clip = sounds[1];
                    audio.volume = 0.9f;
                    audio.loop = true;
                    audio.Play();
                }
            }

            if (stage == 3)
            {
                fireRate = 0.3f;
                Transform right = GameObject.Find("StarSparrow_Weapon_Right").GetComponent<Transform>();
                GameObject rightBullet = Instantiate(bullets[1], right.position, spawn.rotation);
                Destroy(rightBullet, 3f);
                nextFire = Time.time + fireRate;
                audio.clip = sounds[1];
                audio.volume = 0.5f;
                audio.loop = true;
                audio.Play();
            }
        }
    }

    public void nextStage()
    {
        if (stage == 0)
        {
            audio.clip = sounds[2];
            audio.volume = 1.2f;
            audio.loop = false;
            audio.Play();
            ParticleSystem ps = GameObject.Find("StarSparrow_Engine_Left").GetComponentInChildren<ParticleSystem>();
            ps.Play();
        }
        if (stage == 1)
        {
            audio.clip = sounds[2];
            audio.loop = false;
            audio.volume = 1.2f;
            audio.Play();
            ParticleSystem ps = GameObject.Find("StarSparrow_Engine_Right").GetComponentInChildren<ParticleSystem>();
            ps.Play();
        }
        if (stage == 2)
        {
            audio.clip = sounds[2];
            audio.loop = false;
            audio.volume = 1.2f;
            audio.Play();
            ParticleSystem ps = GameObject.Find("StarSparrow_Weapon_Left").GetComponentInChildren<ParticleSystem>();
            ps.Play();
        }
        if (stage == 3)
        {
            audio.clip = sounds[2];
            audio.loop = false;
            audio.volume = 1.2f;
            audio.Play();
            ParticleSystem ps = GameObject.Find("StarSparrow_Weapon_Right").GetComponentInChildren<ParticleSystem>();
            ps.Play();
        }
        stage++;
    }

    void Update()
    {
        base.Update();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            healthComp.takeDamage(bullet.damage, player, gameObject);
            if (healthComp.getHealth() <= 0.0f && stage == 3)
                Destroy(gameObject);
            if (healthComp.getHealth() <= 0.0f && stage == 0)
            {
                nextStage();
                healthComp.Heal();
            }
            if (healthComp.getHealth() <= 0.0f && stage == 1)
            {
                nextStage();
                healthComp.Heal();
            }
            if (healthComp.getHealth() <= 0.0f && stage == 2)
            {
                nextStage();
                healthComp.Heal();
            }
           

            Destroy(other.gameObject);
        }
    }

}
