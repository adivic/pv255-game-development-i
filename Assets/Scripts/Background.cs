﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, 0, -0.01f);
        if(transform.position.z <= -25)
        {
            Destroy(gameObject);
            GameObject g = Instantiate(gameObject, new Vector3(0, -6, 50), transform.rotation);
            g.AddComponent<Background>();
        }

    }
}
