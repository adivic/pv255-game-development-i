﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Player_UI : MonoBehaviour
{
    public Text[] scoreText;
    public Text waveText;
    public GameObject gameOverMenu;
    public GameMode gameMode;
    public GameObject winMenu;

    private void Start()
    {
        foreach (Text score in scoreText)
            score.text = PlayerPrefs.GetInt("Score", 0).ToString();
        
    }

    public void setScoreText(float newScore)
    {
        foreach (Text score in scoreText) {
            score.text = newScore.ToString();
        }
    }

    public void ShowGameOver() { 
        gameMode.ClearScene();
        gameOverMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    public void UpdateWaveText(int newWave) {
        waveText.text = "Wave: " + newWave;
    }

    public void HideGameOver() { 
        gameOverMenu.SetActive(false);
        gameObject.SetActive(true);
    }

    public void ShowWinMenu()
    {
        winMenu.SetActive(true);
        gameObject.SetActive(false);
        foreach (Text score in scoreText)
            score.gameObject.SetActive(false);
    }

    public void HideWinMenu()
    {
        winMenu.SetActive(false);
        gameObject.SetActive(true);
        foreach (Text score in scoreText)
            score.gameObject.SetActive(true);
    }
}
