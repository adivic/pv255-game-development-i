﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody bullet;
    public float speed;
    public float damage;

    void Start()
    {
        bullet = GetComponent<Rigidbody>();
        bullet.velocity = transform.forward * speed;
    }
}
