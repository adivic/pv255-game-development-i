﻿using UnityEngine;
using UnityEngine.UI;

public class HealthComponent : MonoBehaviour
{
    float health;
    Slider slider;
    public float defaultHealth;

    void Start()
    {
        setHealth(defaultHealth);
        slider = GetComponentInChildren<Slider>();

        slider.maxValue = (int)health;
        slider.value = (int)health;
    }

    public void takeDamage(float healthDelta, GameObject DamageCauser, GameObject DamagedActor)
    {
        health -= healthDelta;
        slider.value = health;

        if(health <= 0.0f)
        {
            if(DamagedActor.tag == "Enemy")
            {
                Player_UI ui = DamageCauser.GetComponent<Player_UI>();
                PlayerState state = DamageCauser.GetComponent<PlayerState>();
                state.addScore(10);
                ui.setScoreText(state.loadScore());
            }

            if (DamagedActor.tag == "Boss")
            {
                EnemyBoss boss = DamagedActor.GetComponent<EnemyBoss>();
                if (boss.stage == 3)
                {
                    Player_UI ui = DamageCauser.GetComponent<Player_UI>();
                    PlayerState state = DamageCauser.GetComponent<PlayerState>();
                    state.addScore(100);
                    ui.setScoreText(state.loadScore());
                }
                
            }

            if (DamagedActor.tag == "Player")
            {
                Player_UI ui = DamagedActor.GetComponent<Player_UI>();
                ui.ShowGameOver();
                PlayerState playerState = DamagedActor.GetComponent<PlayerState>();
                playerState.saveScore();
            }
        }
    }

    public void Heal() { 
        health = defaultHealth;
        slider.value = defaultHealth;
    }

    public float getHealth() { return health; }
    public void setHealth(float h) { defaultHealth = h; health = h; }
}
