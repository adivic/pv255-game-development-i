﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Schema;
using UnityEngine;

[System.Serializable]
public struct ScreenSize
{
    public float scr_xmin, scr_xmax, scr_zmin, scr_zmax;
}

public class PlayerController : MonoBehaviour
{
    Rigidbody player;
    public Item[] items;
    public float speed = 10;
    public float tilt;
    public GameObject bullet;
    public Transform spawn;
    ScreenSize screenSize;
    PlayerState state;
    Player_UI playerUI;
    HealthComponent healthComp;
    bool bHasDoubleFire = false; 
    bool bDamageIncreased = false;
    float fireRate = 0.4f;
    float nextFire;
    AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Rigidbody>();
        state = GetComponent<PlayerState>();
        playerUI = GetComponent<Player_UI>();
        healthComp = GetComponent<HealthComponent>();

        Vector3 pos = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>()
            .ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));
        float offsetX = 0.5f;
        screenSize.scr_xmax = pos.x - offsetX;
        screenSize.scr_xmin = -pos.x + offsetX;
        screenSize.scr_zmax = pos.y / 2.5f;
        screenSize.scr_zmin = -pos.y / 4;

        audio = GetComponent<AudioSource>();
        checkUpgrades();
    }

    void Update()
    {
        if (Input.GetButton("Fire1")) {
            if (Time.time > nextFire) {
                if (bHasDoubleFire) {
                    Transform leftWing = GameObject.Find("StarSparrow_PlasmaLeft").transform;
                    GameObject leftBullet = Instantiate(bullet, leftWing.position, leftWing.rotation);
                    Destroy(leftBullet, 2);
                    Transform RightWing = GameObject.Find("StarSparrow_PlasmaRight").transform;
                    GameObject rightBullet = Instantiate(bullet, RightWing.position, RightWing.rotation);
                    if(bDamageIncreased) {
                        rightBullet.GetComponent<Bullet>().damage += 15;
                        leftBullet.GetComponent<Bullet>().damage += 15;
                    }
                    audio.Play();
                    Destroy(rightBullet, 2);
                } else {
                    GameObject firedBullet = Instantiate(bullet, spawn.position, spawn.rotation);
                    if (bDamageIncreased)
                        firedBullet.GetComponent<Bullet>().damage += 15;
                    audio.Play();
                    // Clean the mess
                    Destroy(firedBullet, 2);
                }
                nextFire = Time.time + fireRate;
            }
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        Vector3 acc = Vector3.zero;
        acc.x = Input.acceleration.x;
        acc.z = Input.acceleration.y;

        if (SystemInfo.deviceType == DeviceType.Desktop) 
            player.velocity = movement * speed;
        else
            player.velocity = acc * speed * 2;

        player.position = new Vector3(Mathf.Clamp(player.position.x, screenSize.scr_xmin, screenSize.scr_xmax), 0.0f, Mathf.Clamp(player.position.z, screenSize.scr_zmin, screenSize.scr_zmax));
        player.rotation = Quaternion.Euler(0.0f, 0.0f, player.velocity.x * -tilt);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "EnemyBullet")
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            Destroy(other.gameObject);
            healthComp.takeDamage(bullet.damage, gameObject, gameObject);
        }
    }

    public ScreenSize getScreenSize() { return screenSize; }

    private void checkUpgrades() {
        if (PlayerPrefs.HasKey("Double Barrel")) {
            bHasDoubleFire = PlayerPrefs.GetInt("Double Barrel") == 1 ? true : false;
            Debug.Log("Barrle" + bHasDoubleFire);
        } if (PlayerPrefs.HasKey("Double Tap")) {
            fireRate = PlayerPrefs.GetInt("Double Tap") == 1 ? 0.15f : 0.4f;
            Debug.Log("Tap " + fireRate);
        } if (PlayerPrefs.HasKey("Health")) {
            float h = PlayerPrefs.GetInt("Health") == 1 ? 160 : 100;
            healthComp.setHealth(h);
            Debug.Log("Health  " + h);
        } if (PlayerPrefs.HasKey("Damage")) {
            bDamageIncreased = PlayerPrefs.GetInt("Damage") == 1 ? true : false;
            Debug.Log("Damage  " + bDamageIncreased);
        }
    }
}
