﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;


public class ItemDisplay : MonoBehaviour
{

    public Item item;

    public Text priceText;
    public Text nameText;
    public Image spriteImg;
    public Toggle toggle;


    // Start is called before the first frame update
    void Start()
    {
        item.bBoutht = Load();
        priceText.text = item.price.ToString();
        nameText.text = item.name;
        spriteImg.sprite = item.spirte;
        toggle.isOn = item.bBoutht;
    }

    public void PurchaseItem()
    {
        int playerScore = PlayerPrefs.GetInt("Score");
        if (playerScore >= item.price && !item.bBoutht)
        {
            playerScore -= item.price;
            PlayerPrefs.SetInt("Score", playerScore);
            toggle.isOn = true;
            item.bBoutht = true;
            GetComponentInParent<Store_UI>().updateScore();
            PlayerPrefs.SetInt(item.name, 1);
        }
        else if (item.bBoutht)
        {
            PlayerPrefs.SetInt("Score", playerScore + item.price);
            toggle.isOn = false;
            item.bBoutht = false;
            GetComponentInParent<Store_UI>().updateScore();
            PlayerPrefs.SetInt(item.name, 0);
        }
    }

    public bool Load()
    {
        if (!PlayerPrefs.HasKey(item.name)) return false;
        int data = PlayerPrefs.GetInt(item.name);
        if (data == 1)
            return true;
        else
            return false;
    }
}
