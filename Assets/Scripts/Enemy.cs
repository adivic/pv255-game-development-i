﻿using System.CodeDom;
using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    protected Rigidbody enemy;
    protected float speed;
    public GameObject player;
    protected HealthComponent healthComp;
    protected float fireRate = 2.0f;
    protected float nextFire;
    protected ScreenSize size; 
    
    public ParticleSystem Explosion;
    public GameObject bullet;
    public Transform spawn;

    protected AudioSource audio;
    protected float randomHeight;

    public GameMode gameMode;

    ParticleSystem particleSystem = new ParticleSystem();

    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<Rigidbody>();
        speed = Random.Range(5, 10);
       
        nextFire = Time.time;
        size = player.GetComponent<PlayerController>().getScreenSize();
        StartCoroutine(Move());
        randomHeight = Random.Range(7, 14);
        healthComp = GetComponent<HealthComponent>();
        fireRate = Random.Range(1.0f, 2.0f);
        audio = GetComponent<AudioSource>();
        particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    void OnDestroy() {
        gameMode.killEnemy();
    }

    protected IEnumerator Move()
    {
        yield return new WaitForSeconds(1.1f);
        while (true)
        {
            if (enemy.transform.position.x > size.scr_xmin && enemy.transform.position.x < size.scr_xmax)
            {
                enemy.velocity = transform.right * -getMovement();
                yield return new WaitForSeconds(0.7f);
            }
            else break;
        }
    }

    private float getMovement()
    {
        float playerX = player.transform.position.x;
        float num = Random.Range(playerX - 0.2f, playerX + 0.2f);
        if (num > size.scr_xmin && num < size.scr_xmax) 
            return num;
         else 
            getMovement();
        return 0.0f;
    }

    protected void Update()
    {
        if(enemy.transform.position.z >= randomHeight) {
            enemy.velocity = transform.forward * 2.0f;
        } else {
            if (enemy.transform.position.x < player.transform.position.x - 1.0f)
                enemy.velocity = transform.right * -Random.Range(0.52f, 1.5f);
            if (enemy.transform.position.x > player.transform.position.x + 1.0f)
                enemy.velocity = transform.right * Random.Range(0.5f, 1.5f); 
        }
        if (enemy.transform.position.x < size.scr_xmin)
            enemy.velocity = transform.right * -2;
        if(enemy.transform.position.x > size.scr_xmax)
            enemy.velocity = transform.right * 2;

        if (enemy.transform.position.z <= 15)
            FireProjectile();
    }

    protected virtual void FireProjectile()
    {
        if (Time.time > nextFire) {
            GameObject firedBullet = Instantiate(bullet, spawn.position, spawn.rotation);
            nextFire = Time.time + fireRate;
            audio.Play();
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Bullet") {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            healthComp.takeDamage(bullet.damage, player, gameObject);
            if (healthComp.getHealth() <= 0.0f) {
                particleSystem.Play();
                Destroy(gameObject, 0.2f);
            }
            Destroy(other.gameObject);
        }
    }
}
