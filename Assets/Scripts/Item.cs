﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.Common;

[CreateAssetMenu(fileName = "StoreItem", menuName = "Items")]
public class Item : ScriptableObject
{
    public new string name;
    public int price;
    public Sprite spirte;
    public string description;
    public bool bBoutht;
}
