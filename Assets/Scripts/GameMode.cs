﻿using System.Collections;
using UnityEngine;


[System.Serializable]
public struct FWave
{
    public AudioClip RoundFinishSound;
    public AudioClip OpeningSound;
    public AudioClip backgroundMusic;
}

public class GameMode : MonoBehaviour
{
    public GameObject player;
    AudioSource audio;
    private int numEnemy = 0;

    public GameObject enemy;
    public Vector3 spawns;
    public FWave waveInfo;
    public GameObject boss;

    public short roundDeliminiter = 10;

    public int numOfEnemiesToSpawn;
    public float spawnTimer = 2;
    public float roundTimer;
    public bool bSpawn = true;

    public GameObject animationObject;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnEnemies());
        Animation anim = animationObject.GetComponent<Animation>();
        anim.Play();
        audio = GetComponent<AudioSource>();
        audio.clip = waveInfo.OpeningSound;
        audio.loop = false;
        audio.volume = 1.0f;
        audio.Play(0);
        Vector3 pos = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>()
           .ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        ScreenSize size = player.GetComponent<PlayerController>().getScreenSize();
        float posY = pos.y + 10.0f;
        spawns = new Vector3(pos.x/2, 0, posY);
    }

	public void killEnemy() {
        numEnemy--;
    }

    IEnumerator spawnEnemies() {
        yield return new WaitForSeconds(2.0f);
        while (true) {
            bool bBossRound = player.GetComponent<PlayerState>().round == roundDeliminiter;
            numOfEnemiesToSpawn = bBossRound ? 1 : numOfEnemiesToSpawn;
            GameObject npcToSpawn = bBossRound ? boss : enemy;
            for (int i = 0; i < numOfEnemiesToSpawn; i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(-spawns.x, spawns.x), spawns.y, spawns.z);
                GameObject e = Instantiate(npcToSpawn, spawnPos, new Quaternion(0.0f, 180.0f, 0.0f, 1));
                Enemy enemy = e.GetComponent<Enemy>();
                enemy.player = player;
                enemy.gameMode = this;
                HealthComponent healthComponent = e.GetComponent<HealthComponent>();
                healthComponent.setHealth(healthComponent.defaultHealth + ((player.GetComponent<PlayerState>().round - 1) * 10));
                numEnemy++;
                yield return new WaitForSeconds(spawnTimer);
            }
            bSpawn = false;
            numOfEnemiesToSpawn = bBossRound ? 1 : (numOfEnemiesToSpawn + 2);
            yield return new WaitUntil(() => numEnemy == 0);
            if(bBossRound) {
                ClearScene();
                player.GetComponent<Player_UI>().ShowWinMenu();
                PlayerPrefs.SetInt("UnlockedLevel", 1);
            }
            startNewRound();
            yield return new WaitForSeconds(roundTimer);
            updatePlayerState();
        }
    }

    public void ClearScene()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject g in enemies)
            g.SetActive(false);
        enemies = GameObject.FindGameObjectsWithTag("EnemyBullet");
        foreach (GameObject g in enemies)
            g.SetActive(false);
        enemies = GameObject.FindGameObjectsWithTag("Bullet");
        foreach (GameObject g in enemies)
            g.SetActive(false);
    }

    private void startNewRound()
    {
        bSpawn = true;
        audio.clip = waveInfo.RoundFinishSound;
        audio.Play();
        numEnemy = 0;
    }

    private void updatePlayerState()
    {
        player.GetComponent<HealthComponent>().Heal();
        player.GetComponent<Player_UI>().UpdateWaveText(player.GetComponent<PlayerState>().updateRound());
    }
}
